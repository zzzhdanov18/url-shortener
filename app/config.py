from pydantic import BaseSettings


class Settings(BaseSettings):
    SECRET_KEY: str
    MONGO_DB: str = "test"
    MONGO_TEST_DB: str = "TEST_DB"
    MONGODB_CONNECTION_URL: str
    REDIS_URL: str

    ACCESS_TOKEN_EXPIRATION_TIME: int = 5 * 24 * 60 * 60  # 5 days
    ALGORITHM = "HS256"

    class Config:
        case_sensitive = True
        env_file = ".env"


settings = Settings()
