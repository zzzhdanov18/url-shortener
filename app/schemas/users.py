from pydantic import BaseModel, Field
from typing import Union


class User(BaseModel):
    email: str = Field(..., title="Email")
    links_list: list = Field(..., title="Links list")
    disabled: Union[bool, None] = None


class UserInDB(User):
    hashed_password: str = Field(..., title="Hashed password")
