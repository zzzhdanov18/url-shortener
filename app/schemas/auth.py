from pydantic import BaseModel, Field
from typing import Union


class Token(BaseModel):
    access_token: str = Field(..., title="Token string")
    token_type: str = Field(..., title="Token type")


class TokenData(BaseModel):
    email: Union[str, None] = None


class Auth(BaseModel):
    email: str = Field(..., title="User login")
    password: str = Field(..., max_length=25, title="User password")
