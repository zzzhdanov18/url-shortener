from pydantic import BaseModel, Field


class PostLink(BaseModel):
    original_link: str = Field(..., title="Original link")


class GetLink(PostLink):
    redirected_link: str = Field(..., title="Redirecred link")
    number_of_clicks: int = Field(..., title="Number of clicks")


class Link(GetLink):
    id: str = Field(..., title="Link's id")
