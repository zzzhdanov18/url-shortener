from fastapi import APIRouter

from app.api.auth import auth_router
from app.api.links import links_router
from app.api.health import health_router

api_router = APIRouter()

api_router.include_router(auth_router, prefix="/auth", tags=["auth"])
api_router.include_router(links_router, prefix="/links", tags=["links"])
api_router.include_router(health_router, prefix="/health", tags=["health"])
