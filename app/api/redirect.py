from fastapi import APIRouter, Depends, HTTPException, status, BackgroundTasks
from fastapi.responses import RedirectResponse

from app.db import get_redis_db, get_db
from app.db.links import Links

redirect_router = APIRouter()


@redirect_router.get("/{hash_link}")
async def redirect(
    hash_link: str,
    background_tasks: BackgroundTasks,
    redis_db=Depends(get_redis_db),
    db=Depends(get_db),
):
    target_link = await redis_db.get(hash_link)
    if target_link:
        background_tasks.add_task(
            Links.increment_cliks_for_link, db=db, redirected_link=hash_link
        )
        return RedirectResponse(target_link)
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="The link does not exist"
        )
