from fastapi import APIRouter, Depends, HTTPException, status

from app.schemas.users import User
from app.schemas.links import PostLink, GetLink, Link
from app.api.auth import get_current_active_user
from app.db import get_db, get_redis_db
from app.db.links import Links, hash_link


links_router = APIRouter()

collection = "users"


@links_router.get("", response_model=list[Link])
async def get_links(current: User = Depends(get_current_active_user)):
    return current.links_list


@links_router.post("", response_model=Link)
async def create_new_link(
    link: PostLink,
    current: User = Depends(get_current_active_user),
    db=Depends(get_db),
    redis_db=Depends(get_redis_db),
):
    user_email = current.email
    current_link = await Links.check_for_existence_by_link(
        db=db, user_email=user_email, url=link.original_link
    )
    if not current_link:
        redirected_link = hash_link(link.original_link)
        new_link = await Links.create_link_for_user(
            db=db,
            user_email=user_email,
            url=link.original_link,
            redirected_link=redirected_link,
        )
        await redis_db.set(redirected_link, link.original_link)
        return new_link
    else:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="This link already exists"
        )


@links_router.delete("/{link_id}")
async def delete_link(
    link_id: str,
    current: User = Depends(get_current_active_user),
    db=Depends(get_db),
    redis_db=Depends(get_redis_db),
):
    user_email = current.email

    current_link = await Links.check_for_existence_by_id(
        db=db, user_email=user_email, id=link_id
    )
    if current_link:
        link_info = await Links.get_info_link_for_user(
            db=db, user_email=user_email, id=link_id
        )
        redirected_link = link_info["redirected_link"]
        original_link = link_info["original_link"]
        await Links.delete_link_for_user(
            db=db, user_email=user_email, url=original_link
        )
        await redis_db.delete(redirected_link)
        return {"status": "DELETED"}
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="The link does not exist"
        )


@links_router.patch("/{link_id}")
async def update_link(
    link_id: str,
    current: User = Depends(get_current_active_user),
    db=Depends(get_db),
    redis_db=Depends(get_redis_db),
):
    user_email = current.email
    current_link = await Links.check_for_existence_by_id(
        db=db, user_email=user_email, id=link_id
    )
    if current_link:
        link_info = await Links.get_info_link_for_user(
            db=db, user_email=user_email, id=link_id
        )

        original_link = link_info["original_link"]
        redirected_link = link_info["redirected_link"]

        new_redirected_link = hash_link(original_link)
        await Links.update_link_for_user(
            db=db,
            user_email=user_email,
            url=original_link,
            redirected_link=new_redirected_link,
        )
        await redis_db.set(new_redirected_link, original_link)
        await redis_db.delete(redirected_link)
        return {"status": "UPDATED"}
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="The link does not exist"
        )


@links_router.get("/{link_id}", response_model=GetLink)
async def get_link_by_id(
    link_id: str, current: User = Depends(get_current_active_user), db=Depends(get_db)
):
    user_email = current.email

    current_link_info = await Links.get_info_link_for_user(
        db=db, user_email=user_email, id=link_id
    )
    if current_link_info:
        return GetLink(**current_link_info)
    else:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail="The link does not exist"
        )
