from app.db import get_db, get_redis_db

from fastapi import status, HTTPException, APIRouter, Depends


health_router = APIRouter()


@health_router.get("", status_code=status.HTTP_200_OK)
async def ping_server(db_client=Depends(get_db), redis_client=Depends(get_redis_db)):
    try:
        await db_client.command("ping")
        await redis_client.ping()
    except Exception:
        raise HTTPException(
            status_code=status.HTTP_503_SERVICE_UNAVAILABLE,
            detail="The database is not available at the moment",
        )
