import logging

from motor.motor_asyncio import AsyncIOMotorClient
from redis.asyncio.client import Redis

from app.config import settings
from app.db.redis import RedisClient


logger = logging.getLogger("events")


class Database:
    client: AsyncIOMotorClient = None


db = Database()
redis = RedisClient()


async def connect_to_mongodb():
    logger.info("Connecting to MongoDB")
    db.client = AsyncIOMotorClient(settings.MONGODB_CONNECTION_URL)
    logger.info("Obtained MongoDB connection")
    logger.info("All DB specific procedures completed")


async def connect_to_redis():
    logger.info("Connecting to Redis")
    redis.client = Redis.from_url(settings.REDIS_URL, decode_responses=True)


async def close_redis_connection():
    logger.info("Closing Redis connections")
    await redis.client.close()


async def close_mongodb_connection():
    logger.info("Closing MongoDB connections")
    db.client.close()


async def get_db():
    return db.client[settings.MONGO_DB]


async def get_redis_db():
    return redis.client


async def get_mongo_client():
    return db.client
