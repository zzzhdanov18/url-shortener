from motor.motor_asyncio import AsyncIOMotorDatabase
from uuid import uuid4
import hashlib
import time

from app.schemas.links import Link
from app.schemas.users import User

collection = "users"


def hash_link(link: str) -> str:
    random_artifact = time.perf_counter()
    hash_object = hashlib.sha256((link + str(random_artifact)).encode())
    hash_str = hash_object.hexdigest()
    return hash_str[:8]


class Links:
    @staticmethod
    async def check_for_existence_by_link(
        db: AsyncIOMotorDatabase, user_email: str, url: str
    ):
        result = await db[collection].find_one(
            {"email": user_email, "links_list": {"$elemMatch": {"original_link": url}}}
        )
        return result

    @staticmethod
    async def check_for_existence_by_id(
        db: AsyncIOMotorDatabase, user_email: str, id: str
    ):
        result = await db[collection].find_one(
            {"email": user_email, "links_list": {"$elemMatch": {"id": id}}}
        )
        return result

    @staticmethod
    async def create_link_for_user(
        db: AsyncIOMotorDatabase, user_email: str, url: str, redirected_link: str
    ):
        new_link = Link(
            id=str(uuid4()),
            original_link=url,
            redirected_link=redirected_link,
            number_of_clicks=0,
        )
        await db[collection].update_one(
            {"email": user_email}, {"$push": {"links_list": new_link.dict()}}
        )
        return new_link

    @staticmethod
    async def update_link_for_user(
        db: AsyncIOMotorDatabase, user_email: str, url: str, redirected_link: str
    ):
        await db[collection].update_one(
            {"email": user_email, "links_list.original_link": url},
            {"$set": {"links_list.$.redirected_link": redirected_link}},
        )

    @staticmethod
    async def delete_link_for_user(db: AsyncIOMotorDatabase, user_email: str, url: str):
        await db[collection].update_one(
            {"email": user_email}, {"$pull": {"links_list": {"original_link": url}}}
        )

    @staticmethod
    async def get_info_link_for_user(
        db: AsyncIOMotorDatabase, user_email: str, id: str
    ):
        res = await db[collection].find_one(
            {"email": user_email, "links_list": {"$elemMatch": {"id": id}}}
        )
        if res:
            target_link_info = [item for item in res["links_list"] if item["id"] == id]
            return target_link_info[0]

    @staticmethod
    def get_hash_by_original(user: User, url: str):
        target_hash_info = [
            item for item in user.links_list if item["original_link"] == url
        ]
        return target_hash_info[0]["redirected_link"]

    @staticmethod
    async def increment_cliks_for_link(db: AsyncIOMotorDatabase, redirected_link: str):
        query = {"links_list.redirected_link": redirected_link}
        await db[collection].update_one(
            query, {"$inc": {"links_list.$.number_of_clicks": 1}}
        )
