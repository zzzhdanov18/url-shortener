from typing import Optional
from motor.motor_asyncio import AsyncIOMotorDatabase

from app.schemas.users import UserInDB


collection = "users"


class Users:
    @staticmethod
    async def get_user_by_email(
        db: AsyncIOMotorDatabase, email: str
    ) -> Optional[UserInDB]:
        user = await db[collection].find_one({"email": email})
        if user:
            return UserInDB(**user)

    @staticmethod
    async def create_user(db: AsyncIOMotorDatabase, email: str, hashed_password: str):
        user = UserInDB(email=email, hashed_password=hashed_password, links_list=[])
        await db[collection].insert_one(user.dict())
