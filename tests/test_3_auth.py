class Header:
    data: dict


header = Header()


def test_signin(client):
    email = "tee22e33ww1"
    password = "te2wes34t33erord"

    response = client.post(
        "/api/auth/signin", json={"email": email, "password": password}
    )
    assert response.status_code == 201
    access_token = response.json()["access_token"]
    header.data = {"Authorization": f"Bearer {access_token}"}

    response = client.post(
        "/api/auth/signin", json={"email": email, "password": password}
    )
    assert response.status_code == 409

    response = client.get("/api/auth/me/", headers=header.data)
    assert response.status_code == 200
    assert response.json()["email"] == email

    response = client.get(f"/api/auth/me/")
    assert response.status_code == 401
