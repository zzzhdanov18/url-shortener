def test_auth_me(client):
    response = client.get("/api/auth/me")
    assert response.status_code == 401


def test_get_all_links(client):
    response = client.get("/api/links")
    assert response.status_code == 401


def test_create_link(client):
    link_inf = {"original_link": "string"}
    response = client.post("/api/links", json=link_inf)
    assert response.status_code == 401


def test_link_info(client):
    response = client.get("/api/links/test")
    assert response.status_code == 401


def test_delete_link(client):
    response = client.delete("/api/links/test")
    assert response.status_code == 401


def test_update_link(client):
    response = client.patch("/api/links/test")
    assert response.status_code == 401
