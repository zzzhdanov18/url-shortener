from app.config import settings
from app.db import get_db, Database, get_redis_db
from app.main import app
from app.db.redis import RedisClient
from app.config import settings

from motor.motor_asyncio import AsyncIOMotorClient
from redis.asyncio.client import Redis
import redis
from fastapi.testclient import TestClient
import pytest, asyncio, pymongo


async_db = Database()
async_db.client = AsyncIOMotorClient(
    settings.MONGODB_CONNECTION_URL,
    # serverSelectionTimeoutMS=10,
)

async_redis = RedisClient()
async_redis.client = Redis.from_url(settings.REDIS_URL, decode_responses=True)


async def override_get_db():
    return async_db.client[settings.MONGO_TEST_DB]


async def override_get_redis_db():
    return async_redis.client


app.dependency_overrides[get_db] = override_get_db
app.dependency_overrides[get_redis_db] = override_get_redis_db


@pytest.fixture(scope="session")
def event_loop(request):
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def client():
    with TestClient(app) as client:
        yield client


@pytest.fixture(scope="session", autouse=True)
def drop_database():
    yield
    client = pymongo.MongoClient(settings.MONGODB_CONNECTION_URL)
    db = client[settings.MONGO_TEST_DB]
    for collection_name in db.list_collection_names():
        db[collection_name].drop()

    redis_db = redis.Redis.from_url(settings.REDIS_URL)
    redis_db.flushdb()
