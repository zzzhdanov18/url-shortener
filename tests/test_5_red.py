from tests.test_3_auth import header
from app.schemas.links import Link

test = Link(
    original_link="https://www.google.ru/",
    number_of_clicks=0,
    redirected_link="",
    id="",
)


def test_link_without_clicks(client):
    response = client.post(
        "/api/links", headers=header.data, json={"original_link": test.original_link}
    )
    json = response.json()
    assert response.status_code == 200
    assert json["number_of_clicks"] == 0

    test.redirected_link = json["redirected_link"]
    test.id = json["id"]


def test_simulate_click(client):
    response = client.get(f"/red/{test.redirected_link}", follow_redirects=True)
    response = client.get(f"/red/{test.redirected_link}", follow_redirects=True)
    response = client.get(f"/red/{test.redirected_link}", follow_redirects=True)
    response = client.get(f"/red/{test.redirected_link}", follow_redirects=True)


def test_get_link_with_clicks(client):
    response = client.get(f"/api/links/{test.id}", headers=header.data)
    assert response.status_code == 200
    json = response.json()
    assert json["number_of_clicks"] == 4
