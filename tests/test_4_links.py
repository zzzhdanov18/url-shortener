from tests.test_3_auth import header
from app.schemas.links import Link

test = Link(
    original_link="https://www.google.ru/",
    number_of_clicks=0,
    redirected_link="",
    id="",
)


def test_empty_links_list(client):
    response = client.get("/api/auth/me/", headers=header.data)
    assert response.status_code == 200
    assert response.json()["links_list"] == []


def test_get_nonexistant_link(client):
    response = client.get("/api/links/plug", headers=header.data)
    assert response.status_code == 404


def test_delete_nonexistant_link(client):
    response = client.delete("/api/links/plug", headers=header.data)
    assert response.status_code == 404


def test_update_nonexistant_link(client):
    response = client.patch("/api/links/plug", headers=header.data)
    assert response.status_code == 404


def test_create_link(client):
    response = client.post(
        "/api/links", headers=header.data, json={"original_link": test.original_link}
    )
    json = response.json()

    assert json["original_link"] == test.original_link
    assert json["number_of_clicks"] == test.number_of_clicks

    test.id = json["id"]
    test.redirected_link = json["redirected_link"]


def test_nonempty_links_list(client):
    response = client.get("/api/auth/me/", headers=header.data)
    assert response.status_code == 200
    assert response.json()["links_list"] == [test]


def test_get_link_by_id(client):
    response = client.get(f"/api/links/{test.id}", headers=header.data)
    assert response.status_code == 200
    json = response.json()

    assert json["original_link"] == test.original_link
    assert json["redirected_link"] == test.redirected_link
    assert json["number_of_clicks"] == test.number_of_clicks


def test_update_link_by_id(client):
    response = client.patch(f"/api/links/{test.id}", headers=header.data)
    assert response.status_code == 200

    response = client.get(f"/api/links/{test.id}", headers=header.data)
    json = response.json()

    assert json["original_link"] == test.original_link
    assert json["redirected_link"] != test.redirected_link
    assert json["number_of_clicks"] == test.number_of_clicks


def test_delete_link_by_id(client):
    response = client.delete(f"/api/links/{test.id}", headers=header.data)
    assert response.status_code == 200

    response = client.get("/api/auth/me/", headers=header.data)
    assert response.status_code == 200
    assert response.json()["links_list"] == []
