FROM python:3.10-slim

WORKDIR /app

COPY . /app

RUN pip install --upgrade pip && pip install -r requirements.txt --no-cache-dir

CMD  sh -c "uvicorn app.main:app --host 0.0.0.0 --port 8000"

EXPOSE 8000